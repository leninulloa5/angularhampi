import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PerfilDoctoresScreenComponent } from './perfil-doctores-screen.component';

describe('PerfilDoctoresScreenComponent', () => {
  let component: PerfilDoctoresScreenComponent;
  let fixture: ComponentFixture<PerfilDoctoresScreenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PerfilDoctoresScreenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PerfilDoctoresScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
