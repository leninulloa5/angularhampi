import { Component } from '@angular/core';
import productList from '../app/DB/dataProducts.json';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'oscus2021';
  productList:{id:number, 
                name:string, 
                banner:string, 
                descripition:string,
                youtubeUrl:string,
                natural:string[],
                juridico:string[]
              }[]=productList;

}
