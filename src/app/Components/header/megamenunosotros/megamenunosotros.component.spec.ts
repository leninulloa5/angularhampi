import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MegamenunosotrosComponent } from './megamenunosotros.component';

describe('MegamenunosotrosComponent', () => {
  let component: MegamenunosotrosComponent;
  let fixture: ComponentFixture<MegamenunosotrosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MegamenunosotrosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MegamenunosotrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
