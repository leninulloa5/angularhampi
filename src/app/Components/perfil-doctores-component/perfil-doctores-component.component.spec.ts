import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PerfilDoctoresComponentComponent } from './perfil-doctores-component.component';

describe('PerfilDoctoresComponentComponent', () => {
  let component: PerfilDoctoresComponentComponent;
  let fixture: ComponentFixture<PerfilDoctoresComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PerfilDoctoresComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PerfilDoctoresComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
