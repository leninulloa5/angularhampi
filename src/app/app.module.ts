import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './Components/header/header.component';
import { FooterComponent } from './Components/footer/footer.component';


import { HomeComponent } from './Screens/home/home.component';

import { SidebarComponent } from './Components/sidebar/sidebar.component';
import { CardsComponent } from './Components/doctor/cards/cards.component';
import { BuscadorComponent } from './Components/doctor/buscador/buscador.component';
import { PerfilDoctoresComponentComponent } from './Components/perfil-doctores-component/perfil-doctores-component.component';
import { PerfilDoctoresScreenComponent } from './Screens/perfil-doctores-screen/perfil-doctores-screen.component';




@NgModule({
  declarations: [

    AppComponent,
    HeaderComponent,
    FooterComponent,


    HomeComponent,

    SidebarComponent,
    CardsComponent,
    BuscadorComponent,
    PerfilDoctoresComponentComponent,
    PerfilDoctoresScreenComponent,



  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent },


      {path: 'perfil-doctores', component: PerfilDoctoresScreenComponent},
    ]),

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
